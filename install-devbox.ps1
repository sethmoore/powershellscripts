# Install Chocolatey
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))

# Install Atom editor
iex 'choco install atom -y'

# Install WinMerge
iex 'choco install winmerge -y'

# Install Git
iex 'choco install git -y'
iex 'git config --global user.email "semoore@cimpress.com"'
iex 'git config --global user.name "Seth Moore"'
iex 'git config --global http.sslVerify "false"'
iex 'git config --global core.editor "atom --wait"'

iex 'git config --global merge.tool winmerge'
iex 'git config --replace --global mergetool.winmerge.cmd "C:/Program Files (x86)/WinMerge/WinMergeU.exe" -e "$LOCAL" "$REMOTE"'
iex 'git config --global mergetool.prompt false'

# Install Ruby
iex 'choco install ruby -y'

# Ruby reqs
iex 'gem install bundler'
iex 'bundle install'

# TODO: Update the Java security certificates to include artifactory
#http://share.vistaprint.net/common/share/IT_Operations/Public/Certs/RootCA/vistaprint_root_ca.cer
