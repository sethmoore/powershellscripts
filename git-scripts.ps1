function prompt {
	
	[string]$drive =([string]$pwd).Substring(0,([string]$pwd).IndexOf("\"))
	write-host $drive -NoNewLine -ForegroundColor White
	
	write-host "\.." -NoNewLine -ForegroundColor White
	
	[string]$shortPath =([string]$pwd).Substring(([string]$pwd).LastIndexOf("\"))
	write-host $shortPath -NoNewLine -ForegroundColor White
	$branch = &git rev-parse --abbrev-ref HEAD
	
	if($branch -ne $null)
	{
		write-host "[" -NoNewLine -ForegroundColor White
		write-host $branch -NoNewLine -ForegroundColor Green
		write-host "]" -NoNewLine -ForegroundColor White
	}
	
	">"
}

function push {
	
	$branch = &git rev-parse --abbrev-ref HEAD
	
	git push origin $branch
}

function pull {
	
	$branch = &git rev-parse --abbrev-ref HEAD
	
	git pull origin $branch
}

function merge-master {
	
	git fetch origin
	
	git merge origin master
}

function rebase-master {
	
	git fetch origin
	
	git rebase origin/master
}

function interactive-rebase {
	
	git fetch origin
	
	git rebase origin/master --interactive
}